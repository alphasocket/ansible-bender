import logging
import os
from ansible_bender.constants import OUT_LOGGER, OUT_LOGGER_FORMAT, TIMESTAMP_FORMAT
import shutil
from ansible_bender.utils import run_cmd

logger = logging.getLogger(__name__)
out_logger = logging.getLogger(OUT_LOGGER)


class PortablePy:
    def __init__(self, builder, build, debug=False, verbose=False, init_logging=True):
        """
        :param debug: bool, provide debug output if True
        :param verbose: bool, print verbose output
        :param init_logging: bool, set up logging if True
        """
        self.verbose = verbose
        self.debug = debug

        self.conf = {
            'controller_tmp_folder': "/opt/portablePy/",
            'container_mount_path': "/opt/portablePy/",
            'container_python_interpreter_path': "/opt/portablePy/bin/python3",
            # TODO: finish interpreter build
            'container_image': "docker.io/alpine:latest",
            'container_create_python_cmd': [
                ["apk", "add", "python3", "--no-cache"],
            ],
        }

        # Prepare portable interpreter
        self._prepare_portablepy()

        # Mount interpreter
        self._mount_portablepy(builder)

        # Set mounted interpreter
        build.python_interpreter = self.conf['container_python_interpreter_path']

    def _prepare_portablepy(self):
        # Prepare interpreter if not ready
        if os.path.exists(self.conf['controller_tmp_folder']):
            try:
                run_cmd(["podman", "run", "--rm",
                         "-v", self.conf['controller_tmp_folder'] + ":" + self.conf['container_mount_path'],
                         self.conf['container_image']] + [self.conf['container_python_interpreter_path'], "--version"],
                        return_output=False, log_stderr=True)
                logger.info("PortablyPy is ready")
                return True
            except Exception as error:
                logger.info("PortablyPy is not ready" + str(error))
                logger.info("Removing broken interpreter in " + self.conf['controller_tmp_folder'])
                shutil.rmtree(self.conf['controller_tmp_folder'])
                logger.info("Please try again")

        logger.info("Building PortablyPy, this might take a while")
        try:
            os.mkdir(self.conf['controller_tmp_folder'])

            run_cmd(["podman", "run", "-d", "--name", "portablepy_generator",
                     "-v", self.conf['controller_tmp_folder'] + ":" + self.conf['container_mount_path'],
                     self.conf['container_image']] + ["sleep", "3600"],
                    return_output=False, log_stderr=True)

            # Run commands
            for command in self.conf['container_image']:
                run_cmd(["podman", "exec", "portablepy_generator"] + command, return_output=False, log_stderr=True)

        except Exception as error:
            print(error)
            shutil.rmtree(self.conf['controller_tmp_folder'])
        finally:
            run_cmd(["podman", "rm", "--force", "portablepy_generator"], return_output=False, log_stderr=True)

    def _mount_portablepy(self, builder):
        logger.info("Mounting portablypy on container")
        builder.add_volume(self.conf['controller_tmp_folder'] + ":" + self.conf['container_mount_path'])
